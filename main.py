import graphene
from fastapi import FastAPI
from starlette.graphql import GraphQLApp
from src.graphql.schema import Query, Mutations

app = FastAPI()

app.add_route("/graphql", GraphQLApp(schema=graphene.Schema(mutation=Mutations, query=Query)))

