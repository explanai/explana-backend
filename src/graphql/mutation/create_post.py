import graphene
from graphql import GraphQLError
from jwt import PyJWTError

import src.database.model.post
import src.database.model.user
from src.service.authentication import decode_access_token
from src.database.database import db
from src.schema.post_schema import PostSchema


class CreatePost(graphene.Mutation):
    class Arguments:
        title = graphene.String(required=True)
        content = graphene.String(required=True)
        token = graphene.String(required=True)

    result = graphene.String()

    @staticmethod
    def mutate(root, info, title, content, token):

        try:
            payload = decode_access_token(data=token)
            username = payload.get("user")

            if username is None:
                raise GraphQLError("There is no username in payload")

        except PyJWTError:
            raise GraphQLError("Invalid credentials")

        user = db.query(src.database.model.user.User).filter(src.database.model.user.User.username == username).first()

        if user is None:
            raise GraphQLError("User does not exist")

        post = PostSchema(title=title, content=content)
        db_post = src.database.model.post.Post(title=post.title, content=post.content)
        db.add(db_post)
        db.commit()
        db.refresh(db_post)
        result = "New post added"
        return CreatePost(result=result)