from datetime import timedelta

import bcrypt
import graphene

import src.database.model.user
from src.service.authentication import create_access_token
from src.database.database import db
from src.schema.user_schema import UserSchema


class AuthenticateUser(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    success = graphene.Boolean()
    token = graphene.String()

    @staticmethod
    def mutate(root, info, username, password):
        user = UserSchema(username=username, password=password)
        db_user = db.query(src.database.model.user.User).filter(
            src.database.model.user.User.username == username).first()
        if bcrypt.checkpw(user.password.encode("utf-8"), db_user.password.encode("utf-8")):
            access_token_expiration = timedelta(minutes=60)
            access_token = create_access_token(data={"user": username}, expires_delta=access_token_expiration)
            success = True
            return AuthenticateUser(success=success, token=access_token)
        else:
            success = False
            return AuthenticateUser(success=success)
