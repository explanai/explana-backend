import bcrypt
import graphene

import src.database.model.user
from src.database.database import db
from src.schema.user_schema import UserSchema


class CreateUser(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    success = graphene.Boolean()

    @staticmethod
    def mutate(root, info, username, password):
        hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())

        # decode the hash to prevent it encode twice
        # https://stackoverflow.com/questions/34548846/flask-bcrypt-valueerror-invalid-
        password_hash = hashed_password.decode("utf8")

        user = UserSchema(username=username, password=password_hash)
        db_user = src.database.model.user.User(username=user.username, password=password_hash)
        db.add(db_user)

        # https://docs.sqlalchemy.org/en/13/faq/sessions.html#this-session-s-transaction-has-been-rolled-back-due-to-a-previous-exception-during-flush-or-similar

        try:
            db.commit()
            db.refresh(db_user)
            success = True
            return CreateUser(success=success)
        except:
            db.rollback()
            raise

        db.close()