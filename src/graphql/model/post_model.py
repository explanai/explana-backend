from graphene_sqlalchemy import SQLAlchemyObjectType

from src.database.model.post import Post


class PostModel(SQLAlchemyObjectType):
    class Meta:
        model = Post