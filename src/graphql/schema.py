import graphene

import src.database.model.post
from src.database.database import db
from src.graphql.model.post_model import PostModel
from src.graphql.mutation.authenticate_user import AuthenticateUser
from src.graphql.mutation.create_post import CreatePost
from src.graphql.mutation.create_user import CreateUser


class Query(graphene.ObjectType):
    all_posts = graphene.List(PostModel)
    post_by_id = graphene.Field(PostModel, post_id=graphene.Int(required=True))

    def resolve_all_posts(self, info):
        query = PostModel.get_query(info)
        return query.all()

    def resolve_post_by_id(self, info, post_id):
        return db.query(src.database.model.post.Post).filter(src.database.model.post.Post.id == post_id).first()


class Mutations(graphene.ObjectType):
    authenticate_user = AuthenticateUser.Field()
    create_new_post = CreatePost.Field()
    create_new_user = CreateUser.Field()
