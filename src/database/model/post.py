from sqlalchemy import Column, Integer, String, DateTime, func

from src.database.database import Base


class Post(Base):
    __tablename__ = "post"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    author = Column(String)
    reference = Column(String)
    content = Column(String)
    time_created = Column(DateTime(timezone=True), server_default=func.now())